<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pegawais', function (Blueprint $table) {
            $table->id();
            $table->string('nik')->unique();
            $table->string('name');
            $table->string('kode_agama');
            $table->string('kode_jabatan');
            $table->date('tgl_pegawai');
            $table->foreign('kode_agama')->references('kode')->on('agamas')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('kode_jabatan')->references('kode')->on('jabatans')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pegawais');
    }
};
