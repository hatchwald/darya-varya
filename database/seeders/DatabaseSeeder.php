<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Jabatan;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        \App\Models\Agama::create([
            'kode' => '100',
            'name' => 'Islam',
        ]);
        \App\Models\Agama::create([
            'kode' => '200',
            'name' => 'Kristen',
        ]);

        Jabatan::create([
            'kode' => 'KD1001',
            'name' => 'HRIS SPV'
        ]);
        Jabatan::create([
            'kode' => 'KD2001',
            'name' => 'TR STAFF'
        ]);
    }
}
