<?php

namespace App\Http\Controllers;

use App\Models\Agama;
use App\Models\Jabatan;
use App\Models\Keluarga;
use App\Models\Pegawai;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $pegawai = Pegawai::with('agama')->get();
        return view('pegawai.index', compact('pegawai'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $agama = Agama::all();
        $jabatan = Jabatan::all();
        return view('pegawai.create', compact('agama', 'jabatan'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            Pegawai::create([
                'nik' => $request->username,
                'name' => $request->name,
                'kode_agama' => $request->agama,
                'kode_jabatan' => $request->jabatan,
                'tgl_pegawai' => $request->tgl_pegawai
            ]);

            DB::commit();
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollBack();
            return back()->with('error', $th->getMessage())->withInput();
        }

        return redirect('/pegawai')->with('success', 'content added');
    }

    /**
     * Display the specified resource.
     */
    public function show(Pegawai $pegawai)
    {
        $agama = Agama::all();
        $jabatan = Jabatan::all();
        $keluarga = Keluarga::where('nik_pegawai', $pegawai->nik)->get();
        return view('pegawai.edit', compact('agama', 'jabatan', 'pegawai', 'keluarga'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Pegawai $pegawai)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Pegawai $pegawai)
    {
        try {
            DB::beginTransaction();
            $pegawai->name = $request->name;
            $pegawai->kode_agama = $request->agama;
            $pegawai->kode_jabatan = $request->jabatan;
            $pegawai->tgl_pegawai = $request->tgl_pegawai;
            $pegawai->save();
            DB::commit();
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollBack();
            return back()->with('error', $th->getMessage())->withInput();
        }
        return redirect('/pegawai')->with('success', 'Pegawai Data updated');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Pegawai $pegawai)
    {
        //
    }

    function delete_pegawai(Request $request)
    {
        try {
            DB::beginTransaction();
            $pegawai = Pegawai::find($request->id);
            $pegawai->delete();
            DB::commit();
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollBack();
            return back()->with('error', $th->getMessage())->withInput();
        }
        return redirect()->back()->with('success', 'deleted pegawai');
    }
}
