<?php

namespace App\Http\Controllers;

use App\Models\Keluarga;
use App\Models\Pegawai;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KeluargaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request)
    {
        $pegawai = Pegawai::all();
        $selected_pegawai = $request->nik;
        $hubungan = new Keluarga();
        $hubungan_arr = $hubungan->hubungan_arr;
        return view('keluarga.create', compact('pegawai', 'selected_pegawai', 'hubungan_arr'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $pegawai = Pegawai::where('nik', $request->username)->first();
            Keluarga::create([
                'nik_pegawai' => $request->username,
                'nama_keluarga' => $request->name,
                'hubungan' => $request->hubungan
            ]);
            DB::commit();
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollBack();
            return back()->with('error', $th->getMessage())->withInput();
        }

        return redirect('/pegawai/' . $pegawai->id)->with('success', 'Family added');
    }

    /**
     * Display the specified resource.
     */
    public function show(Keluarga $keluarga)
    {
        $pegawai = Pegawai::all();
        $hubungan = new Keluarga();
        $hubungan_arr = $hubungan->hubungan_arr;
        return view('keluarga.edit', compact('keluarga', 'pegawai', 'hubungan_arr'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Keluarga $keluarga)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Keluarga $keluarga)
    {
        try {
            DB::beginTransaction();
            $pegawai = Pegawai::where('nik', $request->username)->first();
            $keluarga->nik_pegawai = $request->username;
            $keluarga->nama_keluarga = $request->name;
            $keluarga->hubungan = $request->hubungan;
            $keluarga->save();
            DB::commit();
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollBack();
            return back()->with('error', $th->getMessage())->withInput();
        }

        return redirect('/pegawai/' . $pegawai->id)->with('success', 'Family Updated');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Keluarga $keluarga)
    {
    }

    function delete_keluarga(Request $request)
    {
        try {
            DB::beginTransaction();
            $keluarga = Keluarga::find($request->id);
            $pegawai = Pegawai::where('nik', $request->username)->first();
            $keluarga->delete();
            DB::commit();
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollBack();
            return back()->with('error', $th->getMessage())->withInput();
        }
        return redirect()->back()->with('success', 'deleted family');
    }
}
