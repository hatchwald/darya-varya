<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    use HasFactory;
    protected $fillable = ['nik', 'name', 'kode_agama', 'kode_jabatan', 'tgl_pegawai'];

    function agama()
    {
        return $this->hasOne(Agama::class, 'kode', 'kode_agama');
    }

    function jabatan()
    {
        return $this->hasOne(Jabatan::class, 'kode', 'kode_jabatan');
    }
}
