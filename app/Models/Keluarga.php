<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Keluarga extends Model
{
    use HasFactory;
    public $hubungan_arr = [
        'ayah' => 'Ayah',
        'ibu' => 'Ibu',
        'adik' => 'Adik',
        'kakak' => 'Kakak',
        'suami' => 'Suami',
        'istri' => 'Istri',
        'anak' => 'Anak'
    ];
    protected $fillable = [
        'nama_keluarga',
        'hubungan',
        'nik_pegawai'
    ];
}
