@extends('layouts.app')
@section('content')
<div class="flex justify-end">
    <a href="/pegawai" class="bg-indigo-600 px-4 py-3 text-center text-sm font-semibold inline-block text-white cursor-pointer uppercase transition duration-200 ease-in-out rounded-md hover:bg-indigo-700 focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-indigo-600 focus-visible:ring-offset-2 active:scale-95">Back To Index</a>
</div>
<div class="mt-3">
    @if ($message = Session::get('error'))
    <div class="font-regular relative mb-4 block w-full rounded-lg bg-gradient-to-tr from-red-600 to-red-400 p-4 text-base leading-5 text-white opacity-100">
        {{$message}}
      </div>
    @endif
</div>
<form class="bg-white px-4 py-3 mt-3" method="POST" action="/pegawai">
    @csrf
    <div class="space-y-12">
      <div class="border-b border-gray-900/10 pb-12">
        <h2 class="text-base font-semibold leading-7 text-gray-900">Pegawai Form</h2>
        <p class="mt-1 text-sm leading-6 text-gray-600">This information will be displayed publicly so be careful what you share.</p>
  
        <div class="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
          <div class="sm:col-span-4">
            <label for="username" class="block text-sm font-medium leading-6 text-gray-900">Nik</label>
            <div class="mt-2">
              <div class="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
                <input type="text" name="username" id="username" autocomplete="username" class="block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6" placeholder="janesmith">
              </div>
            </div>
          </div>
  
          
        </div>
      </div>
  
      <div class="border-b border-gray-900/10 pb-12">
  
        <div class="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
          <div class="sm:col-span-6">
            <label for="first-name" class="block text-sm font-medium leading-6 text-gray-900">Name</label>
            <div class="mt-2">
              <input type="text" name="name" id="name" autocomplete="given-name" class="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
            </div>
          </div>
  
          <div class="sm:col-span-6">
            <label for="agama" class="block text-sm font-medium leading-6 text-gray-900">Agama</label>
            <div class="mt-2">
              <select id="agama" name="agama" autocomplete="agama-name" class="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:max-w-xs sm:text-sm sm:leading-6">
                @foreach ($agama as $item)
                <option value="{{$item->kode}}">{{$item->name}}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="sm:col-span-6">
            <label for="jabatan" class="block text-sm font-medium leading-6 text-gray-900">Jabatan</label>
            <div class="mt-2">
              <select id="jabatan" name="jabatan" autocomplete="jabatan-name" class="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:max-w-xs sm:text-sm sm:leading-6">
                @foreach ($jabatan as $item)
                    <option value="{{$item->kode}}">{{$item->name}}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="sm:col-span-6">
            <label for="jabatan" class="block text-sm font-medium leading-6 text-gray-900">Tanggal Pegawai</label>
            <div class="mt-2">
              <input type="date" name="tgl_pegawai" id="">
            </div>
          </div>
  
  
  
  
        </div>
      </div>
  
    </div>
  
    <div class="mt-6 flex items-center justify-end gap-x-6">
      <a href="/pegawai" class="text-sm font-semibold leading-6 text-gray-900">Cancel</a>
      <button type="submit" class="rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600">Save</button>
    </div>
  </form>
  
@endsection