@extends('layouts.app')
@section('content')
<div class="flex justify-center">
    <p class="text-xl text-white font-bold">Pegawai</p>
</div>
<div class="mt-3">
  @if ($message = Session::get('success'))
        <div class="font-regular relative mb-4 block w-full rounded-lg bg-gradient-to-tr from-green-600 to-green-400 p-4 text-base leading-5 text-white opacity-100">
          {{$message}}
        </div>
    @endif
</div>
<div class="flex justify-end mt-3">
    <a href="/pegawai/create" class="bg-indigo-600 px-4 py-3 text-center text-sm font-semibold inline-block text-white cursor-pointer uppercase transition duration-200 ease-in-out rounded-md hover:bg-indigo-700 focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-indigo-600 focus-visible:ring-offset-2 active:scale-95">add new</a>
</div>
 <p>
    
     <div class="mt-5 text-white">
        <table class="border-separate border border-slate-500">
            <thead>
              <tr>
                <th class="border border-slate-600 px-3 py-3">NIK</th>
                <th class="border border-slate-600 px-3 py-3">NAME</th>
                <th class="border border-slate-600 px-3 py-3">Agama</th>
                <th class="border border-slate-600 px-3 py-3">Jabatan</th>
                <th class="border border-slate-600 px-3 py-3">Tgl Pegawai</th>
                <th class="border border-slate-600 px-3 py-3">Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($pegawai as $item)
                <tr>
                  <td class="border border-slate-700 px-3 py-3">{{$item->nik}}</td>
                  <td class="border border-slate-700 px-3 py-3">{{$item->name}}</td>
                  <td class="border border-slate-700 px-3 py-3">{{$item->agama->name}}</td>
                  <td class="border border-slate-700 px-3 py-3">{{$item->jabatan->name}}</td>
                  <td class="border border-slate-700 px-3 py-3">{{$item->tgl_pegawai}}</td>
                  <td class="border border-slate-700 px-3 py-3"><a href="/pegawai/{{$item->id}}">Edit</a> | <a href="/pegawai-delete/{{$item->id}}" onclick="return confirm('Are you sure?')">Delete</a></td>
                </tr>
              @endforeach
             
            </tbody>
          </table>    
    </div>   
 </p>
@endsection