<?php

use App\Http\Controllers\AgamaController;
use App\Http\Controllers\JabatanController;
use App\Http\Controllers\KeluargaController;
use App\Http\Controllers\PegawaiController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resources([
    'agama' => AgamaController::class,
    'jabatan' => JabatanController::class,
    'pegawai' => PegawaiController::class,
    'keluarga' => KeluargaController::class
]);

Route::get('keluarga-delete/{id}', [KeluargaController::class, 'delete_keluarga']);
Route::get('pegawai-delete/{id}', [PegawaiController::class, 'delete_pegawai']);
